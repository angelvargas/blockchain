import hashlib
import json
import requests
from textwrap import dedent
from time import time
from uuid import uuid4
from flask import Flask, jsonify, request
from urllib.parse import urlparse

class Blockchain(object):
    def __init__(self):
        self.chain = []
        self.current_transactions = []
        self.nodes = set()
        #crea el genesis block
        self.new_block(previous_hash=1, proof=100)

    def new_block(self, proof, previous_hash):
        """
        crea un nuevo bloque en el blockchain
        parametro proff:la prueba dada por la prueba del trabajo del algoritmo
        parametro previous_hash:hash del bloque anterior
        return: <dict> nuevo bloque
        """
        block = {
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'transactions': self.current_transactions,
            'proof': proof,
            'previous_hash': previous_hash or self.hash(self.chain[-1])
        }
        #resetea la actual lista de transacciones
        self.current_transactions = []

        self.chain.append(block)
        return block

    def new_transaction(self, sender, recipient, amount):
        """
        crea una nueva transaccion para ir al siguiente bloque minado
        parametro sender: <str> Dirección del remitente
        parametro recipient: <str> Dirección del Destinatario
        parametro amount: <int> Cantidad
        return: <int> El índice del Bloque que contendrá esta transacción
        """
        self.current_transactions.append({
            'sender': sender,
            'recipient': recipient,
            'amount': amount
        })
        return self.last_block['index'] + 1

    @property
    def last_block(self):
        return self.chain[-1]

    @staticmethod
    def hash(block):
        """
        crea un SHA-256 hash del bloque
        parametro block: <dict> Block
        :return: <str>
        """
        block_string = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(block_string).hexdigest()

    def proof_of_work(self, last_proof):
        """
        Simple Proof of Work Algorithm
        Encuentre un número p' tal que el hash (pp') contenga 4 ceros iniciales, 
        donde p es el anterior p'
        p es proof anterior, y p' es nuevo proof
        parametro last_proof: <int>
        return: <int> int
        """
        proof = 0
        while self.valid_proof(last_proof, proof) is False:
            proof += 1
        return proof

    @staticmethod
    def valid_proof(last_proof, proof):
        """
        Valida proof: ¿el hash(last_proof, proof) contiene 4 ceros iniciales?
        parametro last_proof: <int> proof anterior
        parametro proof: <int> proof actual
        return: <bool> Verdadero si es correcto, Falso si no
        """
        guess = f'{last_proof}{proof}'.encode()
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash[:6] == "000000"
    
    def register_node(self, address):
        """
        agregar un nuevo nodo a la lista de nodos
        parametro address: <str> direccion de nodo node. Eg. 'http://192.168.0.5:5000'
        return: None
        """
        parsed_url = urlparse(address)
        total_nodes = len( self.nodes )
        if( total_nodes < 10):
            self.nodes.add(parsed_url.netloc)
        else:
            return False
        return True

    def valid_chain(self, chain):
        """
        Determine si una cadena de bloques dada es válida
        parametro chain: <list> a blockchain
        return: <bool>True si es valido, false si no
        """
        last_block = chain[0]
        current_index = 1
        while current_index < len(chain):
            block = chain[current_index]
            print(f'{last_block}')
            print(f'{block}')
            print("\n-----------\n")
             # revisa que el hash de el  block es correcto
            if block['previous_hash'] != self.hash(last_block):
                return False
            # revisa que el Proof of Work es correcto
            if not self.valid_proof(last_block['proof'], block['proof']):
                return False
            last_block = block
            current_index += 1
        return True

    def resolve_conflict(self):
        """
        Este es nuestro algoritmo de consenso, resuelve conflictos.
        Reemplazando nuestra cadena por la más larga de la red.
        : return: <bool> True si nuestra cadena fue reemplazada, False si no
        """
        neighbours = self.nodes
        new_chain = None
        # Solo buscamos cadenas más largas que la nuestra.
        max_length  = len(self.chain)
        # Agarre y verifique las cadenas de todos los nodos de nuestra red.
        for node in neighbours:
            try:
                response = requests.get(f'http://{node}/chain')
                if response.status_code == 200:
                    length = response.json()['length']
                    chain = response.json()['chain']
                    # Comprobar si la longitud es más larga y si la cadena es válida.
                    if length > max_length and self.valid_chain(chain):
                        max_length = length
                        new_chain = chain
            except requests.exceptions.RequestException as e:
                print(f'http://{node}/chain')
                print(e)
                print("\n-----------\n")
            #print(f'http://{node}/chain')
            
                    
        # Reemplace nuestra cadena si descubrimos una nueva cadena válida más larga que la nuestra.
        if new_chain:
            self.chain = new_chain
            return True
        
        return False

#instacia de nuestro nodo
app = Flask(__name__)

#genera una direccion global y unica para este nodo
node_identifier = str(uuid4()).replace('-', '')

#instancia del blockchain
blockchain = Blockchain()
"""
Nuestro punto final de minería es donde ocurre la magia, 
y es fácil. Tiene que hacer tres cosas:
Calcular la Prueba de Trabajo
Recompense al minero (nosotros) agregando una transacción otorgándonos 1 moneda
Forja el nuevo Bloque añadiéndolo a la cadena.
"""

@app.route('/mine', methods=['GET'])
def mine():
    # Ejecutamos el algoritmo de prueba de trabajo para obtener la siguiente prueba ...
    last_block = blockchain.last_block
    last_proof = last_block['proof']
    proof = blockchain.proof_of_work(last_proof)

    #Debemos recibir una recompensa por encontrar la prueba
    #El remitente es "0" para indicar que este nodo ha extraído una nueva moneda
    blockchain.new_transaction(sender=0, recipient=node_identifier, amount=1)

    previous_hash = blockchain.hash(last_block)
    block = blockchain.new_block(proof, previous_hash)
    response = {
        'message': 'Nuevo Bloque Forjado',
        'index': block['index'],
        'transactions': block['transactions'],
        'proof': block['proof'],
        'previous_hash': block['previous_hash']
    }
    return jsonify(response), 200

@app.route('/transactions/new', methods=['POST'])
def new_transaction():
    values = request.get_json()
    # Comprobar que los campos requeridos están en los datos POST'ed
    required = ['sender', 'recipient', 'amount']
    if not all(k in values for k in required):
        return 'Missing values', 400
    #crea una nueva transaccion
    index = blockchain.new_transaction(
        values['sender'], values['recipient'], values['amount'])
    response = {'message': f'Transaccion sera agregada al bloque {index}'}
    return jsonify(response), 201

@app.route('/chain', methods=['GET'])
def full_chain():
    response = {
        'chain': blockchain.chain,
        'length': len(blockchain.chain)
    }
    return jsonify(response), 200

@app.route('/nodes/register', methods=['POST'])
def register_nodes():
    values = request.get_json()

    nodes = values.get('nodes')
    if nodes is None:
        return "Error:Por favor, suministre una lista válida de nodos", 400
    message  = []
    for node in nodes:
        if( blockchain.register_node(node) ):
            message.append(f'{node}'+' ha sido agregado')
        else:
            message.append(f'{node}'+' no ha sido agregado, maximo 10')
    
    response = {
        'message' : message,
        'total_nodes' : list(blockchain.nodes)
    }
    return jsonify(response), 201

@app.route('/nodes/resolve', methods=['GET'])
def consesus():
    replaced = blockchain.resolve_conflict()
    if replaced:
        response = {
            'message' : 'Nuestra cadena fue reemplazada',
            'new_chain' : blockchain.chain
        }
    else:
        response = {
            'message' : 'Nuestra cadena es autoritaria.',
            'chain' : blockchain.chain
        }
    
    return jsonify(response), 200

@app.route('/getblock', methods=['POST'])
def get_block():
    values = request.get_json()
    bloque = values['block']

    try:
        _bloque = blockchain.chain[bloque-1]
        _hash = blockchain.hash(_bloque)
        mensaje = 'Bloque obtenido'
    except IndexError:
        _bloque = []
        _hash = ''
        mensaje = 'Bloque Vacio'

    response = {
        'block' : _bloque,
        'hash' : _hash,
        'message' : mensaje
    }
    return jsonify(response), 200
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)